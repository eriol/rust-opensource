# 0.1.1 (2016-06-20)

- Move repository under Open Source Initiative organization. Thanks to paultag
  to make this real! :)
- Build and upload documentation.

# 0.1.0 (2016-06-17)

- First public release.
